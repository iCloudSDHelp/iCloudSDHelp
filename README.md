```bash
$ git clone https://gitlab.com/iCloudSDHelp/iCloudSDHelp && cd iCloudSDHelp
$ make && make install
```


```bash
$ iCloudSDHelp
Logging to: /var/log/iCloudSDHelp.log
```

If you'd like the application to run on startup, run the `startup` make target:

```bash
$ sudo make startup
```

## Uninstallation

You can completely remove the application from your system (including the startup daemon) by running the following command (logs will not be deleted):

```bash
$ sudo make uninstall
```

### Optional Parameters

You can pass in two optional parameters to the program. The `clear` option will clear the logs at the default location. Any other argument passed in will be used as the path to the log file for that process. See below:

```bash
# Clear the logfile.
$ iCloudSDHelp clear
Logfile cleared.

# Specify a logfile location.
$ iCloudSDHelp ~/logfile.txt
Logging to: /Users/Name/logfile.txt
```
